package primer.service

import com.twitter.finagle.Service
import com.twitter.finagle.http._
import com.twitter.util.Future
//import io.netty.handler.codec.http.{HttpResponse, HttpResponseStatus, HttpVersion}

class SimpleService extends Service[Request, Response]{
  override def apply(request: Request): Future[Response] = {

    val response = request.method match {
      case  Method.Post =>
        request.uri match {
          case "/" =>
            val response = Response(Version.Http11, Status.Ok)
            response.contentString = "Hello Stranger !"
            response

          case "/greet" =>
            val name = request.getContentString
            val response = Response(Version.Http11, Status.Ok)
            response.contentString = s"Hello $name !"

            response

          case _ =>
            Response(Version.Http11, Status.NotFound)
        }

      case Method.Get=>
        request.uri match {
          case "/" =>
            val response = Response(Version.Http11, Status.Ok)
            response.contentString = "Thanks Stranger !!"
            response

          case "/greet" =>
            val name = request.getContentString
            val response = Response(Version.Http11, Status.Ok)
            response.contentString = s"Thanks $name !"
            Thread.sleep(3000)
            response

          case _ =>
            Response(Version.Http11, Status.NotFound)
        }
    }

    Future.value(response)
  }
}
