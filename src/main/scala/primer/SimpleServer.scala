package primer

import java.net.InetSocketAddress

import com.twitter.finagle.builder.ServerBuilder
import com.twitter.finagle.Http
import primer.filter.SimpleFilter
import primer.service.SimpleService

object SimpleServer extends App {

  val simpleService = new SimpleService
  val simpleFilter = new SimpleFilter
  val filteredSrvc = simpleFilter andThen  simpleService
  val address = new InetSocketAddress(8180)

  ServerBuilder()
    .stack(Http.server)
    .bindTo(address)
    .name("MyHttpServer")
    .build(filteredSrvc)
}
