package primer.filter

import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

import com.twitter.finagle.http._
import com.twitter.finagle.{Filter, Service}
import com.twitter.util.Future
import org.apache.log4j.LogManager

class SimpleFilter extends Filter[Request, Response, Request, Response] {

  private val log = LogManager.getLogger(this.getClass)
  val dtf = DateTimeFormatter.ofPattern("hh:mm:ss")

  override def apply(request: Request, service: Service[Request, Response]): Future[Response] = {
    println(s"Logging REQUEST content in filter: ${request.contentString} at ${dtf.format(LocalDateTime.now)}")
    val resp = service(request)
    println(s"Logging RESPONSE in filter: $resp at ${dtf.format(LocalDateTime.now)}")
    resp
  }
}
