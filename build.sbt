name := "finagle-primer-server"

version := "0.1"

scalaVersion := "2.13.3"


//finagle
libraryDependencies += "com.twitter" %% "finagle-core" % "20.10.0"
libraryDependencies += "com.twitter" %% "finagle-http" % "20.10.0"
libraryDependencies += "log4j" % "log4j" % "1.2.17"
